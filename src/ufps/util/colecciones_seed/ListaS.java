/**
 * ---------------------------------------------------------------------
 * $Id: ListaS.java,v 2.0 2013/08/23
 * Universidad Francisco de Paula Santander
 * Programa Ingenieria de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;
import java.util.Objects;

/**
 * Implementacion de la Clase Lista Simple para el manejo de Listas Encadenadas.
 *
 * @param <T> Tipo de datos a almacenar en la lista.
 * @author Marco Adarme
 * @version 2.0
 */
public class ListaS<T> implements Iterable<T> {

    ////////////////////////////////////////////////////////////
    // ListaS - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Representea el Nodo cabecera de la Lista
     */
    private Nodo<T> cabeza;

    /**
     * Representa el tamaño de la Lista
     */
    private int tamanio;

    ////////////////////////////////////////////////////////////
    // ListaS - Implementacion de Metodos //////////////////////
    ////////////////////////////////////////////////////////////
    /**
     * Constructor de la Clase Lista Simple Enlazada, por defecto la cabeza es
     * NULL. <br>
     * <b>post: </b> Se construyo una lista vacia.
     */
    public ListaS() {
        this.cabeza = null;
        this.tamanio = 0;
    }

    /**
     * Metodo que inserta un Elemento al Inicio de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al inicio de la Lista.<br>
     *
     * @param x Informacion que desea almacenar en la Lista. La informacion debe
     * ser un Objeto.
     */
    public void insertarAlInicio(T x) {
        this.cabeza = new Nodo<T>(x, this.cabeza);
        this.tamanio++;
    }

    /**
     * Metodo que inserta un Elemento al Final de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al final de la Lista.<br>
     *
     * @param x Información que desea almacenar en la Lista.
     */
    public void insertarAlFinal(T x) {
        if (this.cabeza == null) {
            this.insertarAlInicio(x);
        } else {
            try {
                Nodo<T> ult = this.getPos(this.tamanio - 1);
                if (ult == null) {
                    return;
                }
                ult.setSig(new Nodo<T>(x, null));
                this.tamanio++;
            } catch (ExceptionUFPS e) {
                System.err.println(e.getMensaje());
            }
        }
    }

    /**
     * Metodo que inserta un Elemento de manera Ordenada desde la cabeza de la
     * Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento en la posicion segun el Orden
     * de la Lista.<br>
     *
     * @param info Información que desea almacenar en la Lista de manera
     * Ordenada.
     */
    public void insertarOrdenado(T info) {
        if (this.esVacia()) {
            this.insertarAlInicio(info);
        } else {
            Nodo<T> x = this.cabeza;
            Nodo<T> y = x;
            while (x != null) {
                Comparable comparador = (Comparable) info;
                int rta = comparador.compareTo(x.getInfo());
                if (rta < 0) {
                    break;
                }
                y = x;
                x = x.getSig();
            }
            if (x == y) {
                this.insertarAlInicio(info);
            } else {
                y.setSig(new Nodo<T>(info, x));
                this.tamanio++;
            }
        }
    }

    /**
     * Metodo que elimina un elemento dada una posición. <br>
     * <b>post: </b> Se elimino el dato en la posicion de la lista indicada.<br>
     *
     * @param i Una posición en la Lista <br>
     * @return El elemento que elimino. Si la posición no es válida retorna
     * NULL.
     */
    public T eliminar(int i) {
        if (this.esVacia()) {
            return null;
        }
        Nodo<T> t = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();
        } else {
            try {

                Nodo<T> y = this.getPos(i - 1);

                t = y.getSig(); // 2,7,null

                y.setSig(t.getSig());

                System.out.println(this.toString());
            } catch (ExceptionUFPS e) {
                System.err.println(e.getMensaje());
                return (null);
            }
        }
        // t.setSig(null);
        this.tamanio--;
        return (t.getInfo());
    }

    /**
     * Metodo que elimina todos los datos de la Lista Simple. <br>
     * <b>post:</b> La Lista Simple se encuentra vacia.
     */
    public void vaciar() {
        this.cabeza = null;
        this.tamanio = 0;
    }

    /**
     * Metodo que retorna el elemento que se encuentre en una posicion dada.
     * <br>
     * <b>post: </b> Se retorno el elemento indicado por la posicion
     * recibida.<br>
     *
     * @param i Una Posición dentro de la Lista. <br>
     * @return El objeto que se encuentra en esa posición. El objeto <br>
     * retorna su valor parametrizada "T". Si la posición no se <br>
     * encuentra en la Lista retorna null.
     */
    public T get(int i) {
        try {
            Nodo<T> t = this.getPos(i);
            return (t.getInfo());
        } catch (ExceptionUFPS e) {
            System.err.println(e.getMensaje());
            return (null);
        }

    }

    /**
     * Metodo que edita el elemento que se encuentre en una posición dada. <br>
     * <b>post: </b> Se edito la informacion del elemento indicado por la
     * posicion recibida.<br>
     *
     * @param i Una Posición dentro de la Lista. <br>
     * @param dato es el nuevo valor que toma el elmento en la lista
     */
    public void set(int i, T dato) {
        try {
            Nodo<T> t = this.getPos(i);
            t.setInfo(dato);
        } catch (ExceptionUFPS e) {
            System.err.println(e.getMensaje());
        }
    }

    /**
     * Metodo que obtiene la cantidad de elementos de la Lista. <br>
     * <b>post: </b> Se retorno el numero de elementos existentes en la
     * Lista.<br>
     *
     * @return int con el tamaño de la lista. Si la Lista esta vacía retorna 0
     */
    public int getTamanio() {
        return (this.tamanio);
    }

    /**
     * Metodo que verifica si la Lista esta o no vacia. <br>
     * <b>post: </b> Se retorno true si la lista se encuentra vacia, false si
     * tiene elementos.<br>
     *
     * @return true si la lista esta vacia , false si contiene elementos.
     */
    public boolean esVacia() {
        return (this.cabeza == null);
    }

    /**
     * Metodo que busca un elemento en la lista si lo encuentra retorna true, de
     * lo contrario false. <br>
     * <b>post: </b> Se retorno true si se encontro el elementos buscado, false
     * si no fue asi.<br>
     *
     * @param info el cual contiene el valor del parametro a buscar en la lista.
     * <br>
     * @return un boolean, si es true encontro el dato en la lista y si es false
     * no lo encontro.
     */
    public boolean esta(T info) {
        return (this.getIndice(info) != -1);
    }

    /**
     * Metodo que crea para la lista simple un elemento Iterator.
     * <b>post: </b> Se retorno un Iterator para la Lista.<br>
     *
     * @return Un iterator tipo <T> de la lista.
     */
    @Override
    public Iterator<T> iterator() {
        return new IteratorLS<T>(this.cabeza) {
        };
    }

    /**
     * Metodo que permite retornar la informacion de una Lista en un Vector.
     * <br>
     *
     * @return Un vector de Objetos con la informacion de cada posicion de la
     * Lista.
     */
    public Object[] aVector() {
        if (this.esVacia()) {
            return (null);
        }
        Object vector[] = new Object[this.getTamanio()];
        Iterator<T> it = this.iterator();
        int i = 0;
        while (it.hasNext()) {
            vector[i++] = it.next();
        }
        return (vector);
    }

    /**
     * Metodo que retorna toda la informacion de los elementos en un String de
     * la Lista. <br>
     * <b>post: </b> Se retorno la representacion en String de la Lista. El
     * String tiene el formato "e1->e2->e3..->en", donde e1, e2, ..., en son los
     * los elementos de la Lista. <br>
     *
     * @return Un String con los datos de los elementos de la Lista
     */
    @Override
    public String toString() {
        if (this.esVacia()) {
            return ("Lista Vacia");
        }
        String r = "";
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            r += x.getInfo().toString() + "->";
        }
        return (r);
    }

    /**
     * Metodo privado de la clase que devuelve al elemento en la posicion. <br>
     * <b>post: </b> Se retorno el Nodo que se encuentra en esa posicion
     * indicada. <br>
     *
     * @param i Una posici�n en la Lista. <br>
     * @return El elemento encontrado. Si la posici�n no es v�lida retorna null
     */
    private Nodo<T> getPos(int i) throws ExceptionUFPS {
        if (this.esVacia() || i > this.tamanio || i < 0) {
            throw new ExceptionUFPS("El índice solicitado no existe en la Lista Simple");
        }
        Nodo<T> t = this.cabeza;
        while (i > 0) {
            t = t.getSig();
            i--;
        }
        return (t);
    }

    /**
     * Metodo que obtiene la posición de un objeto en la Lista. <br>
     * <b>post: </b> Se retorno la posicion en la que se encuentra el dato
     * buscado.
     *
     * @param info Objeto que se desea buscar en la Lista <br>
     * @return un int con la posición del elemento,-1 si el elemento no se
     * encuentra en la Lista
     */
    public int getIndice(T info) {
        int i = 0;
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            if (x.getInfo().equals(info)) {
                return (i);
            }
            i++;
        }
        return (-1);
    }

    public T getMenor() {
        if (this.esVacia()) {
            throw new RuntimeException("Es imposible encontrar el menor");
        }
        Nodo<T> men = this.getMenorNodo(this.cabeza);
        return men.getInfo();
    }

    private Nodo<T> getMenorNodo(Nodo<T> men) {

        Nodo<T> pos = men.getSig();

        while (pos != null) {

            int c = comparador(men.getInfo(), pos.getInfo());
            if (c > 0) {
                men = pos;
            }
            pos = pos.getSig();

        }

        return men;
    }

    private Nodo<T>[] getMenorNodo2(Nodo<T> men) {
        Nodo<T> nodos[];
        nodos = new Nodo[2];

        Nodo<T> pos = men.getSig();

        while (pos != null) {

            int c = comparador(men.getInfo(), pos.getInfo());
            if (c > 0) {
                if(pos.getSig() == null){
                
                //nodos[0]
                
                }
                
                men = pos;

            }

            pos = pos.getSig();

        }

        nodos[1] = men;
        return nodos;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.cabeza);
        hash = 43 * hash + this.tamanio;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        Nodo<T> cab1 = this.cabeza;
        Nodo<T> cab2 = other.cabeza;

        while (cab1 != null && cab2 != null) {
            T info1 = cab1.getInfo();
            T info2 = cab2.getInfo();

            if (!info1.equals(info2)) {
                return false;
            }
            cab1 = cab1.getSig();
            cab2 = cab2.getSig();
        }

        return cab1 == null && cab2 == null;
    }
    public void eliminarMayor(){
        
       boolean entrar = true;
        for(Nodo<T> x=this.cabeza;x!=null&&entrar;x=x.getSig()){
             Nodo<T> []antMayor = getMayorDato(x);
             Nodo<T> anterior = antMayor[0];
             Nodo<T> mayor = antMayor[1];
             for(Nodo<T> y=this.cabeza;y!=null;y=y.getSig()){
             if(comparador(mayor.getInfo(), y.getInfo())==0&&entrar){
                 elimineElMayor(mayor,y.getSig(),anterior,x);
                 entrar = false;
             }
           }
        }
    }
    
    private void elimineElMayor(Nodo<T> mayor, Nodo<T> y, Nodo<T> anterior, Nodo<T> x){
        Nodo<T> aux = x;
        Nodo<T> aux2 = aux.getSig();
        mayor=null;
        anterior.setSig(y);
        aux.setSig(aux2);
        System.out.println(this.toString());   
    }
    
    private Nodo<T>[] getMayorDato(Nodo<T> actual) 
    {
        Nodo<T> [] antMayor = new Nodo [2];
        Nodo<T> aux =this.cabeza;
               
         for(Nodo<T> x=actual.getSig();x!=null;x=x.getSig()){                
                                
             if(this.comparador(actual.getInfo(),x.getInfo())<0){               
                actual=x;            
                antMayor[1]=actual;
                if(antMayor[1]==aux.getSig()){
                    antMayor[0]=aux;                    
                }
            }                     
            aux=aux.getSig();
        }
         return antMayor;
    }
    private void intercambiar(Nodo<T> primero, Nodo<T> segundo) {
        T info1 = primero.getInfo();
        primero.setInfo(segundo.getInfo());
        segundo.setInfo(info1);

    }
    
    public Boolean repetidos(){
    
        for(Nodo<T>x=cabeza;x!=null;x=x.getSig())
        {
            if(infoRepetido(x.getInfo()))
                return true;
        }
    return false;
    }
    
    
    public Boolean existeInfo(T info) {

        Nodo<T> aux = cabeza;

        while (aux != null) {
            if (comparador(aux.getInfo(), info) == 0) {
                return true;
            }
            aux = aux.getSig();
        }
        return false;
    }
    
    public Boolean infoRepetido(T info) {

        Nodo<T> aux = cabeza;
        int contador=0;
        
        while (aux != null) 
        {
            if (comparador(aux.getInfo(), info) == 0) 
                contador++;
            
            aux = aux.getSig();
        }
        
        if(contador==2)
        {
            return true;
        }
       return false;
    }
    
    
    
    public void eliminarHastaMayor() throws ExceptionUFPS {
        Nodo<T> t = cabeza;
        while (cabeza != t) {
            t = t.getSig();

        }
        cabeza = t;

    }

   public void cambiar(int posNodo1, int posNodo2) throws ExceptionUFPS
    {

        Nodo<T> anterior,nodoActual;
        
        if(cabeza.getSig() == null){
            return;
        
        }else{
            
        nodoActual=cabeza;
        anterior=null;
    
        while(nodoActual!=null && nodoActual.getInfo()!= getPos(posNodo1).getInfo())
        {
        anterior=nodoActual;
        nodoActual=nodoActual.getSig();
        }
        
        Nodo<T>nodo1=nodoActual; /// (0,1) 3,4
        Nodo<T>antNodo1=anterior;
    
           nodoActual=cabeza;
           anterior=null;

               while(nodoActual!=null &&nodoActual.getInfo()!=getPos(posNodo2).getInfo())
               {
               anterior=nodoActual;
               nodoActual=nodoActual.getSig();
               }       
           Nodo<T>nodo2=nodoActual;
           Nodo<T>antNodo2=anterior;
           //pX  pY
          //...->5->6...
         //pY  pX
        //...->6->5...
           if(nodo1.getSig() == nodo2 && antNodo2==nodo1 || nodo2.getSig() == nodo1 && antNodo1 == nodo2)
           {
              

               Nodo<T> pe;
               if(nodo1.getSig() == nodo2 && antNodo2==nodo1)
               {
                   
                   pe = nodo2.getSig(); ///->... 36->9->7->99->62->8->               
                   antNodo1.setSig(nodo2);//5
                   nodo1.setSig(pe);
                   nodo2.setSig(antNodo2);
                   
               }else
               {
                   pe = nodo1.getSig(); ///->... 36->9->7->99->62->8->
                   antNodo2.setSig(nodo1);//5
                   nodo2.setSig(pe);
                   nodo1.setSig(antNodo1);
               
               }
              
           }
           else
           {
           Nodo<T>temp;
           temp=nodo2.getSig();


           nodo2.setSig(nodo1.getSig());
           nodo1.setSig(temp);


           if(antNodo1 == null){
           cabeza=nodo2;
           antNodo2.setSig(nodo1);
           }

           if(antNodo2 == null){
           cabeza=nodo1;
           antNodo1.setSig(antNodo2);
           }
           
           if(antNodo1!=null &&antNodo2!=null){

           antNodo1.setSig(nodo2);
           antNodo2.setSig(nodo1);
    
    }
    }
        }    
    }

    private int cantidadNodosAnterioresA(Nodo<T> nodo) {
        int contador = 0;
        for (Nodo<T> x = cabeza; x != nodo; x = x.getSig()) {
            contador++;
        }
        return contador;
    }

    private void compararConNodosAnteriores(Nodo<T> nodo) {
        //3,33,23,67,4,42,14,423
        //3,23,33,67,4,42,14,423
        //x
        //nodo
        Nodo<T> prueba = cabeza;
        while (prueba != nodo) {
            prueba = prueba.getSig();

        }

        for (Nodo<T> x = prueba; x != null; x = anterior(x)) {
           
            int c = comparador(nodo.getInfo(), x.getInfo());

            if (c > 0) {

                intercambiar(prueba, nodo);

            }
        }

    }
    public void ordenarInserccionPorNodos() throws ExceptionUFPS { //incompleto

        for(Nodo<T>x=cabeza;x!=null;x=x.getSig()){
            
        Nodo<T> antX = anterior(x);
            int c = comparador(x.getInfo(), antX.getInfo());            
            if (c < 0) {
                intercambiar(antX, x);
                if (anterior(x) != null) {
                    compararConNodosAnteriores(x);
                }
            }
        
        }
         
       
    }
    
    public void ordenarInserccionPorInfos() {
 //10,23,56,34,0,21
 
        for( Nodo<T> actual=cabeza.getSig(),puntero;actual!=null;)
        {
            puntero=cabeza;
            
            while(puntero!=actual)        
            {                     //5 - 4
                int c = comparador(puntero.getInfo(),actual.getInfo());
                if(c>0)
                    intercambiar(actual,puntero);
                
                else
                    puntero = puntero.getSig();
                
            }  
            
            actual=actual.getSig();
       
        
        }
        
    }
    
    public void ordenarSeleccionPorInfos() throws ExceptionUFPS {

        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            Nodo<T> men = getMenorNodo(x);
            int c = comparador(men.getInfo(), x.getInfo());

            if (c < 0) {
                intercambiar(men, x);
            }
        }

    }
    public void ordenarSeleccionPorNodos() throws ExceptionUFPS {

    if (cabeza.getSig() == null) 
            return;
         else {

            for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {

                Nodo<T> men = getMenorNodo(x);
                int c = comparador(men.getInfo(), x.getInfo());
                if (c < 0) {
                    cambiar(getIndice(x.getInfo()), getIndice(men.getInfo()));
                    x = cabeza;
                }

            }
        }
    }

    private Nodo<T> llegarHastaNodo(int contador, Nodo<T> x) throws ExceptionUFPS {

        while (x != getPos(contador)) 
        {
            x = x.getSig();
        }
        return x.getSig();
    }

    public T anteriorInfo(T info) {

        Nodo<T> n1;
        for (n1 = cabeza; n1.getInfo() != info; n1 = n1.getSig()) {

            if (comparador(n1.getSig().getInfo(), info) == 0) {
                return n1.getInfo();

            }
        }
        return null;

    }

    private Nodo<T> anterior(Nodo<T> nodo) 
    {

        if (nodo == cabeza) 
            return null;
         else {
            Nodo<T> n1;
            for (n1 = cabeza; n1 != nodo; n1 = n1.getSig()) {               
                if (n1.getSig() == nodo) 
                    return n1;
            }
            return n1;
        }
    }


    public int comparador(T info1, T info2) {

        Comparable c = (Comparable) info1;
        return c.compareTo(info2);

    }

}//Fin de la Clase ListaS
