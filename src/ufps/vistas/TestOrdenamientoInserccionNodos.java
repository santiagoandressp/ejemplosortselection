/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author santi
 */
public class TestOrdenamientoInserccionNodos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ExceptionUFPS {
        ListaS<Integer> l =new ListaS();
        
        l.insertarAlInicio(78);
        l.insertarAlInicio(2);
        l.insertarAlInicio(3);
        l.insertarAlInicio(45);
        l.insertarAlInicio(56);
        l.insertarAlInicio(5);
        l.insertarAlInicio(3);
        l.insertarAlInicio(0);
        l.insertarAlInicio(26);
        l.insertarAlInicio(43);
        l.insertarAlInicio(79);
        l.insertarAlInicio(12);
        System.out.println("Original=  "+l.toString());
        l.ordenarInserccionPorNodos();//falta
        System.out.println("Ordenada por inserccion por infos=  "+l.toString());
    }
    
}
