/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author santi
 */
public class Experimento5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
          ListaS l =new ListaS();
      
        for (int i = 0; i < 200000; i++) {
            l.insertarAlInicio(i+"dato"+i);
            l.insertarAlFinal("Nombre"+i);
            
            if(i%2==0){
            l.insertarAlFinal("Santiago");
            l.insertarAlInicio(i);
            }
            

    }   
        try{
        System.out.println(l.toString());
        l.ordenarSeleccionPorInfos();
            System.out.println(l.toString());
        
        }catch(ExceptionUFPS e){
            System.out.println(e.getMessage());
        
        }
        
        long inicio =  System.nanoTime();
        
        l.ordenarInserccionPorInfos();
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
        
        
    }
    
}
