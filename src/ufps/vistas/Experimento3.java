/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author santi
 */
public class Experimento3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
          ListaS<Integer> l =new ListaS();
      
        for (int i = 0; i < 5000000; i++) {
            l.insertarAlFinal(i);

    }
        long inicio =  System.nanoTime();
        
        l.ordenarInserccionPorInfos();
        
        long ultimo = System.nanoTime();
        
        double dif=(double)(ultimo-inicio)*1.0e-9;
        System.out.println("El programa duró:"+dif+"s en ejecutarse");
        
        
    }
    
}
