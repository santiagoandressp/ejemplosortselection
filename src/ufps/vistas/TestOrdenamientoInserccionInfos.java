/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author santi
 */
public class TestOrdenamientoInserccionInfos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ListaS<Integer> l =new ListaS();
        
        l.insertarAlInicio(34);
        l.insertarAlInicio(2);
        l.insertarAlInicio(3);
        l.insertarAlInicio(23);
        l.insertarAlInicio(72);
        l.insertarAlInicio(1);
        l.insertarAlInicio(0);
        l.insertarAlInicio(123);
        
        System.out.println("Original=  "+l.toString());
        l.ordenarInserccionPorInfos();
        System.out.println("Ordenada por inserccion por infos=  "+l.toString());
        
        
    }
    
}
