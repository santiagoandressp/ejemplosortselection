/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import ufps.modelo.Persona;
import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author MADARME
 */
public class TestSort {
    
    public static void main(String[] args) throws ExceptionUFPS {
        ListaS<String> l=new ListaS();
        l.insertarAlFinal("GABRIEL");
        l.insertarAlFinal("JUAN");
        l.insertarAlFinal("ANA");
        l.insertarAlFinal("FARID");
        
        System.out.println("El menor de la lista es:"+l.getMenor());
        
        
        ListaS<Persona> personas=new ListaS();
        personas.insertarAlFinal(new Persona("Marco",1000));
       
        personas.insertarAlFinal(new Persona("Mariana",100));
        personas.insertarAlFinal(new Persona("Pepe",101));
         personas.insertarAlFinal(new Persona("Maria",10));
        
        System.out.println("El menor de la lista es:"+personas.getMenor());
        
        
        System.out.println("Lista Desordenada:"+personas.toString());
        
        System.out.println("Lista Ordenada:"+personas.toString());
    }
    
}
