/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Arrays;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MADARME
 */
public class ListaSIT {

    public ListaSIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of insertarAlInicio method, of class ListaS.
     */
    /**
     * Test of testsortSelection method, of class ListaS.
     *
     * @throws ufps.util.colecciones_seed.ExceptionUFPS
     */
    @Test
    public void testsortSelection() throws ExceptionUFPS {
        System.out.println("");
        System.out.println("testSort");
        System.out.println("Escenario 1");
        ListaS<Integer> desordenada = new ListaS();
        ListaS<Integer> ordenada = new ListaS();

        for (int i = 0; i < 58; i++) {
            int numero = (int) (Math.random() * 89);
            if (!desordenada.existeInfo(numero)) {
                desordenada.insertarAlFinal(numero);
                ordenada.insertarOrdenado(numero);
            }

        }

        Boolean valido = false;
        desordenada.ordenarSeleccionPorNodos();
        if (ordenada.equals(desordenada)) {
            System.out.println("Ordenada=" + ordenada.toString());
            System.out.println("Lista ordenada=" + desordenada.toString());
            valido = true;

        }

        assertTrue(valido);

    }

    @Test
    public void aVector() {
        System.out.println("");
        System.out.println("A vector");
        System.out.println("Escenario 1");
        ListaS<String> lis = new ListaS();
        String[] vector = {"a0", "a1", "a2"};

        for (int i = 0; i < 3; i++) {
            lis.insertarAlFinal("a" + i);
        }
        Object[] v = lis.aVector();

        boolean verdad = false;
        for (int i = 0; i < 3; i++) {

            if (v[i].equals(vector[i])) {
                verdad = true;
            } else {
                verdad = false;
            }
        }

        assertTrue(verdad);

    }

    @Test
    public void aVector2() {
        System.out.println("");
        System.out.println("A vector");
        System.out.println("Escenario 2");
        ListaS<Integer> lis = new ListaS();
        int[] vector = {2,3,4};

        for (int i = 2; i < 5; i++) {
            lis.insertarAlFinal(i);
        }
        Object[] v = lis.aVector();
        
        boolean verdad = false;
        for (int i = 0; i < 3; i++) {

            if (v[i].equals(vector[i])) {
                verdad = true;
            } else {
                verdad = false;
            }
        }

        assertTrue(verdad);

    }

    @Test
    public void testsortSelection2() throws ExceptionUFPS {
        System.out.println("");
        ListaS<String> desordenada;
        ListaS<String> ordenada;

        System.out.println("testSort");
        System.out.println("Escenario 2");
        desordenada = new ListaS();
        ordenada = new ListaS();

        String[] nombres = {"santiago", "laura", "ricardo", "lucas", "juan"};

        for (int i = 0; i < nombres.length; i++) {
            ordenada.insertarOrdenado(nombres[i]);
            desordenada.insertarAlFinal(nombres[i]);
        }

        Boolean valido = false;
        desordenada.ordenarSeleccionPorNodos();
        if (ordenada.equals(desordenada)) {
            System.out.println("Ordenada=" + ordenada.toString());
            System.out.println("Lista ordenada=" + desordenada.toString());
            valido = true;

        }

        assertTrue(valido);

    }

    @Test
    public void testRemove() {
        System.out.println("");
        System.out.println("testRemove");
        System.out.println("Escenario 1");
        ListaS<Integer> l = new ListaS();
        ListaS<Integer> listaQueEspero = new ListaS();

        for (int i = 0; i < 24; i++) {

            if (i == 0 || i == 23) {

                l.insertarAlFinal(i);

            } else {
                l.insertarAlFinal(i);
                listaQueEspero.insertarAlFinal(i);

            }

        }

        l.eliminar(0);
        l.eliminar(22);

        if (l.equals(listaQueEspero)) {
            System.out.println("Ordenada=" + l.toString());
            System.out.println("Lista ordenada=" + listaQueEspero.toString());
            assertTrue(true);
        } else {
            assertTrue(false);
        }

    }

    @Test
    public void testRemove2() {
        System.out.println("");
        System.out.println("testRemove");
        System.out.println("Escenario 2");
        ListaS<String> l = new ListaS();
        ListaS<String> listaQueEspero = new ListaS();

        String[] paises = {"Colombia", "Estados Unidos", "Alemania", "Perú", "Bolivia", "Chile"};

        for (int i = 0; i < paises.length; i++) {

            if (i == 0) {

                l.insertarAlFinal(paises[i]);
            } else {
                listaQueEspero.insertarAlFinal(paises[i]);
                l.insertarAlFinal(paises[i]);
            }

        }
        String datoEliminar = l.get(0);

        l.eliminar(0);

        System.out.println("Lista normal=  " + l.toString());
        System.out.println("Lista Con dato " + datoEliminar + " eliminado=  " + listaQueEspero.toString());
        if (l.equals(listaQueEspero)) {
            assertTrue(true);
        } else {
            assertTrue(false);
        }
    }

    @Test
    public void equals() {
        System.out.println("");
        System.out.println("Test Equals");
        System.out.println("Escenario 1");
        ListaS<Integer> l = new ListaS();
        ListaS<Integer> l2 = new ListaS();
        l2.insertarAlInicio(1);
        l2.insertarAlInicio(2);
        l2.insertarAlInicio(3);
        l2.insertarAlInicio(4);
        l2.insertarAlInicio(5);
        l2.insertarAlInicio(6);
        l2.insertarAlInicio(7);
        for (int i = 1; i < 8; i++) {
            l.insertarAlInicio(i);
        }

        System.out.println("Lista 1=" + l.toString());
        System.out.println("Lista que espero =" + l2.toString());

        if (l.equals(l2)) {
            assertTrue(true);
        } else {
            assertTrue(false);
        }

    }

    @Test
    public void equals2() {
        System.out.println("");
        System.out.println("Test Equals");
        System.out.println("Escenario 2");
        ListaS<Integer> l = new ListaS();
        ListaS<Integer> l2 = new ListaS();

        l2.insertarAlInicio(6);
        l2.insertarAlInicio(5);
        l2.insertarAlInicio(4);
        l2.insertarAlInicio(3);
        l2.insertarAlInicio(2);
        l2.insertarAlInicio(1);
        for (int i = 6; i >= 1; i--) {
            l.insertarAlInicio(i);
        }

        System.out.println("Lista 1=" + l.toString());
        System.out.println("Lista que espero =" + l2.toString());

        if (l.equals(l2)) {
            assertTrue(true);
        } else {
            assertTrue(false);
        }

    }

}
